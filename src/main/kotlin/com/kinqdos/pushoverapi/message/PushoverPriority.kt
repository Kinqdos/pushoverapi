package com.kinqdos.pushoverapi.message

/**
 * Copyright (c) 2021 Kinqdos
 * All rights reserved
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @project PushoverAPI
 * @author Kinqdos
 */
enum class PushoverPriority(val code: Int) {
    /**
     * No notification
     */
    Lowest(-2),

    /**
     * No sound or vibration, but notification
     */
    Low(-1),

    /**
     * Default
     * Generate sound, vibration and notification
     */
    Normal(0),

    /**
     * Bypass quiet hours & red background
     */
    High(1),

    /**
     * Need acknowledge. First user in group cancels retries for the whole group
     * Need "retry" parameter -> Retries every x >= 30 seconds
     * Need "expire" parameter -> Retries expires after x <= 10800 (3 Hours) seconds
     * //TODO CallBack & receipt
     */
    Emergency(2)
}