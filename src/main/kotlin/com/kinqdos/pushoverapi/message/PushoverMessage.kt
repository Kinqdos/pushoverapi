package com.kinqdos.pushoverapi.message

import com.kinqdos.pushoverapi.Pushover

/**
 * Copyright (c) 2021 Kinqdos
 * All rights reserved
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @project PushoverAPI
 * @author Kinqdos
 */
class PushoverMessage() {

    var token = "" //Application Token
    var receiver = "" //User/Group
    var content = "" //Message Content

    var device = "" //Device to send to
    var title = "" //Title of the message
    var url = "" //Link if message gets extended by clicking on it
    var url_title = "" //Shown Text instead of url
    lateinit var priority: PushoverPriority
    lateinit var sound: PushoverSound
    var timestamp = -1 //Unix Timestamp to display to the user instead of message received by api

    constructor(token: String, receiver: String, content: String) : this() {
        this.token = token
        this.receiver = receiver
        this.content = content
    }

    constructor(token: String, receiver: String, title: String, content: String) : this(token, receiver, content) {
        this.title = title
    }

    fun build(builder: PushoverMessage.() -> Unit): PushoverMessage {
        this.builder()
        return this
    }

    internal fun hasPriority() = this::priority.isInitialized

    internal fun hasSound() = this::sound.isInitialized

    fun send() = Pushover.sendMessage(this)

}