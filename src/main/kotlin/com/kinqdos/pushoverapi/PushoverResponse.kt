package com.kinqdos.pushoverapi

import com.kinqdos.library.fromJsonMap
import java.util.*

/**
 * Copyright (c) 2021 Kinqdos
 * All rights reserved
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @project PushoverAPI
 * @author Kinqdos
 */
class PushoverResponse(code: Int, message: String, private val header: MutableMap<String, MutableList<String>>) {

    val status = if (code == 200) Status.Success else if (code in 400..499) Status.InvalidInput else Status.ServerDown
    val error: String = try {
        val parsed = message.fromJsonMap<Any>()
        (parsed["errors"] as ArrayList<*>)[0] as String
    } catch (e: Exception) {
        ""
    }

    val remainingMessages: Int
        get() {
            if (status != Status.Success) throw PushoverException("Remaining messages is only available if status = Success")
            return header["X-Limit-App-Remaining"]!![0].toInt()
        }
    val limitReset: Date
        get() {
            if (status != Status.Success) throw PushoverException("Limit reset is only available if status = Success")
            return Date(header["X-Limit-App-Reset"]!![0].toLong())
        }
    val limit: Int
        get() {
            if (status != Status.Success) throw PushoverException("Limit is only available if status = Success")
            return header["X-Limit-App-Limit"]!![0].toInt()
        }

    enum class Status {
        Success,
        InvalidInput,
        ServerDown
    }

}