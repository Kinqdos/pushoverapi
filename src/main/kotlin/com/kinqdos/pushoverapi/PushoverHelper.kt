package com.kinqdos.pushoverapi

import java.io.IOException
import java.io.PrintWriter
import java.net.URL
import java.util.*
import javax.net.ssl.HttpsURLConnection

/**
 * Copyright (c) 2021 Kinqdos
 * All rights reserved
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @project PushoverAPI
 * @author Kinqdos
 */
internal object PushoverHelper {

    private const val messageURL = "https://api.pushover.net/1/messages.json"

    fun sendMessage(body: String): PushoverResponse {
        try {
            val connection = makeRequest(messageURL, "POST", body)
            val input = try {
                connection.inputStream
            } catch (e: IOException) {
                connection.errorStream
            }
            return PushoverResponse(connection.responseCode, Scanner(input).use { it.nextLine() }, connection.headerFields)
        } catch (e: Exception) {
            throw PushoverException("Unable to connect to Pushover service")
        }
    }

    private fun makeRequest(url: String, method: String, body: String?): HttpsURLConnection {
        val con = URL(url).openConnection() as HttpsURLConnection
        con.requestMethod = method
        con.doOutput = true
        //Write body
        if (body != null) {
            PrintWriter(con.outputStream, true).use { it.println(body) }
        }
        return con
    }

}