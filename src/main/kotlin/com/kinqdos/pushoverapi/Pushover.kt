package com.kinqdos.pushoverapi

import com.kinqdos.library.extensions.implode
import com.kinqdos.pushoverapi.message.PushoverMessage
import java.util.*

/**
 * Copyright (c) 2021 Kinqdos
 * All rights reserved
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @project PushoverAPI
 * @author Kinqdos
 */
object Pushover {

    fun sendMessage(message: PushoverMessage): PushoverResponse {
        val fields = ArrayList<String>()
        if (message.token.isNotBlank()) fields.add("token=${message.token}")
        if (message.receiver.isNotBlank()) fields.add("user=${message.receiver}")
        if (message.content.isNotBlank()) fields.add("message=${message.content}")
        if (message.device.isNotBlank()) fields.add("device=${message.device}")
        if (message.title.isNotBlank()) fields.add("title=${message.title}")
        if (message.url.isNotBlank()) fields.add("url=${message.title}")
        if (message.url_title.isNotBlank()) fields.add("url_title=${message.url_title}")
        if (message.hasPriority()) fields.add("priority=${message.priority.code}")
        if (message.hasSound()) fields.add("sound=${message.sound.name}")
        if (message.timestamp != -1) fields.add("timestamp=${message.timestamp}")
        return PushoverHelper.sendMessage(fields.implode("&"))
    }

    fun sendMessage(builder: PushoverMessage.() -> Unit) = sendMessage(PushoverMessage().build(builder))

}