group = "com.kinqdos"
version = "1.1"

plugins {
    id("maven-publish")
    kotlin("jvm") version "1.8.20"
}

tasks {
    val taskGroup = ".dev"

    register("installLocal") {
        group = taskGroup
        dependsOn(getByName("publishToMavenLocal"))
    }
    register("deployRemote") {
        group = taskGroup
        dependsOn(getByName("publish"))
    }
}

repositories {
    mavenCentral()
    maven("https://repo.kinqdos.de/releases")
}

dependencies {
    implementation("com.kinqdos", "KotlinLibrary", "1.3.9")
}

publishing {
    publications {
        publishing {
            create<MavenPublication>("kinqdos-repo") {
                from(components["java"])
            }
        }
    }

    repositories {
        maven {
            credentials {
                val repoUser: String? by project
                val repoPassword: String? by project
                username = repoUser ?: ""
                password = repoPassword ?: ""
            }
            url = uri("https://repo.kinqdos.de/releases")
        }
    }
}